

Did not notice that there was any problem with git. Then as I was about to push I saw that it was not possible.
So I had to create a new repository to push to, and remove the git from the cloned repo. In this way I lost
several branches and commits that you could have seen.