class Calculator {
    currentEntry;
    firstEntryNumber;
    secondEntryNumber;

    chosenControl;

    controlActive;
    result;

    constructor() {
        this.currentEntry;
        this.result = 0;
        this.firstEntryNumber = 0;
        this.secondEntryNumber = 0;
        this.controlActive = false;
        this.chosenControl = "";
    }

    setCurrentEntry(entry) {
        this.currentEntry = entry;
    }

    addToCurrentEntry(entry) {
        this.currentEntry += entry;
    }

    add(number1, number2) {
        this.result = (number1 + number2);
    }

    subtract(number1, number2) {
        this.result = (number1 - number2);
    }


    multiply(number1, number2) {
        this.result = (number1 * number2);
    }

    clear() {
        this.firstEntryNumber = 0;
        this.secondEntryNumber = 0;
        this.currentEntry = 0;
        this.controlActive = false;
        this.chosenControl = "";
    }

    calculate() {
        this.controlActive = false;

        if (this.chosenControl === "+") {
            this.add(this.firstEntryNumber, this.secondEntryNumber);
            this.clear();
        }
        if (this.chosenControl === "-") {
            console.log("-");
            this.subtract(this.firstEntryNumber, this.secondEntryNumber);
            this.clear();
        }
        if (this.chosenControl === "*") {
            this.multiply(this.firstEntryNumber, this.secondEntryNumber);
            this.clear();
        } else {
            console.log("No control specified");
        }
    }


}

export default Calculator;