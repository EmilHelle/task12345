import '../styles/index.scss';
import * as moment from 'moment';
import Calculator from './task2/Calculator';
import RPSgame from './task3/RPSgame';

console.log('webpack starterkit');
console.log(moment().format('LLL'));

/**
 * For more information on using Moment head to https://momentjs.com/
 */

//task1
//clock
document.getElementById("currentTime").innerText = moment().format("dddd, MMMM Do YYYY");





//task2
//calculator
var calculator = new Calculator();


//input
//currentEntry
var currentEntryElement = document.getElementById("currentEntry");
currentEntryElement.value = calculator.currentEntry;
//result
var resultEntryElement = document.getElementById("resultEntry");
resultEntryElement.value = calculator.result;

//keypad
var keypadElement = document.getElementById("calculatorKeypad");

var buttonKeys = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
buttonKeys.forEach(number => {
    var keyButton = document.createElement("button");
    keyButton.className = "keyButton";
    keyButton.innerText = number;
    keypadElement
        .appendChild(
            keyButton
        );
});

//handle keypad keyButton events
var keyButton = document.getElementsByClassName("keyButton");

keyButton.forEach(element => {
    element.addEventListener("click", function () {

        var currentEntry = 0;

        if (calculator.controlActive) {
            if (calculator.secondEntryNumber === 0) {
                calculator.secondEntryNumber = parseInt(element.innerText);
                calculator.currentEntry = calculator.secondEntryNumber;
            } else {
                calculator.secondEntryNumber = "" + calculator.secondEntryNumber + parseInt(element.innerText);
                calculator.currentEntry = calculator.secondEntryNumber;
            }
        } else {
            if (calculator.firstEntryNumber === 0) {
                calculator.firstEntryNumber = parseInt(element.innerText);
                calculator.currentEntry = calculator.firstEntryNumber;
            } else {
                calculator.firstEntryNumber = "" + calculator.firstEntryNumber + parseInt(element.innerText);
                calculator.currentEntry = calculator.firstEntryNumber;
            }
        }
        // calculator.currentEntry = currentEntry;
        currentEntryElement.value = calculator.currentEntry;
        console.log("firstNumber", calculator.firstEntryNumber);
        console.log("secondNumber", calculator.secondEntryNumber);
        console.log("currentEntry", calculator.currentEntry);
    })
});


//handle controlButton events
var controlButtons = document.getElementsByClassName("controlsButton");
controlButtons.forEach(controlButton => {
    controlButton.addEventListener("click", function () {
        calculator.controlActive = true;
        calculator.currentEntry = 0;
        calculator.chosenControl = controlButton.innerText;
    })
});

//handle clearButton event
var clearButton = document.getElementById("clearButton");
clearButton.addEventListener("click", function(){
    calculator.clear();
    currentEntryElement.value = calculator.currentEntry;
});

//handle calculateButton event
var calculateButton = document.getElementById("calculateButton");
calculateButton.addEventListener("click", function(){
    calculator.calculate();
    resultEntryElement.value = calculator.result;
});


//rps game

var rpsGame = new RPSgame();

//feedback element
var feedbackElement = document.getElementById("rpsGameFeedback");
var cpuFeedbackElement = document.getElementById("rpsGameCPUFeedback");

//handle rockButton
var rockButton = document.getElementById("rockButton");
rockButton.addEventListener("click", function(){
    rpsGame.setUserChosenHand("rock");
    rpsGame.calculate();
    cpuFeedbackElement.innerText = "CPU choice: " + rpsGame.cpuChosenHand;
    if(rpsGame.victory === true){
        feedbackElement.innerText = "VICTORY!";
    }else{
        feedbackElement.innerText = "DEFEAT!";
    }
});

//handle paperButton
var paperButton = document.getElementById("paperButton");
paperButton.addEventListener("click", function(){
    rpsGame.setUserChosenHand("paper");
    rpsGame.calculate();
    cpuFeedbackElement.innerText = "CPU choice: " + rpsGame.cpuChosenHand;
    if(rpsGame.victory === true){
        feedbackElement.innerText = "VICTORY!";
    }else{
        feedbackElement.innerText = "DEFEAT!";
    }
});

//handle scissorsButton
var scissorsButton = document.getElementById("scissorsButton");
scissorsButton.addEventListener("click", function(){
    rpsGame.setUserChosenHand("scissors");
    rpsGame.calculate();
    cpuFeedbackElement.innerText = "CPU choice: " + rpsGame.cpuChosenHand;
    if(rpsGame.victory === true){
        feedbackElement.innerText = "VICTORY!";
    }else{
        feedbackElement.innerText = "DEFEAT!";
    }
});



