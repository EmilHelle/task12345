
//fibonacci
var i, n = 50, t1 = 0, t2 = 1, nextTerm;
for (i = 1; i <= n; ++i) {
    console.log("%d, ", t1);
    nextTerm = t1 + t2;
    t1 = t2;
    t2 = nextTerm;
}
