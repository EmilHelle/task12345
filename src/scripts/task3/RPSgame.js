class RPSgame {

    possibleHands;

    userChosenHand;
    cpuChosenHand;
    
    victory;

    constructor() {
        this.possibleHands = ["rock", "paper", "scissors"];
        this.victory = false;
        this.userChosenHand = "";
        this.cpuChosenHand = this.possibleHands[Math.floor(Math.random()*this.possibleHands.length)];
    }

    setUserChosenHand(chosenHand) {
        this.userChosenHand = chosenHand;
    }

    clear() {
        this.victory = false;
        this.userChosenHand = "";
        this.cpuChosenHand = this.possibleHands[Math.floor(Math.random()*this.possibleHands.length)];
    }

    calculate() {

        if (this.userChosenHand === "rock" && this.cpuChosenHand === "rock") {
            this.victory = false;
        }
        if (this.userChosenHand === "paper" && this.cpuChosenHand === "paper") {
            this.victory = false;
        }
        if (this.userChosenHand === "scissors" && this.cpuChosenHand === "scissors") {
            this.victory = false;
        }

        if (this.userChosenHand === "rock" && this.cpuChosenHand === "paper") {
            this.victory = false;
        }
        if (this.userChosenHand === "rock" && this.cpuChosenHand === "scissors") {
            this.victory = true;
        }

        if (this.userChosenHand === "paper" && this.cpuChosenHand === "rock") {
            this.victory = true;
        }
        if (this.userChosenHand === "paper" && this.cpuChosenHand === "scissors") {
            this.victory = false;
        }

        if (this.userChosenHand === "scissors" && this.cpuChosenHand === "rock") {
            this.victory = false;
        }
        if (this.userChosenHand === "scissors" && this.cpuChosenHand === "paper") {
            this.victory = true;
        }
        
    }


}

export default RPSgame;